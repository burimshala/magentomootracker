<?php namespace Moosend\MagentoMooTracker\Model;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Moosend\TrackerFactory;

class CustomerLoginObserver implements ObserverInterface
{

    /**
     * @var ScopeConfigInterface
     */
    private $configInterface;

    /**
     * @var TrackerFactory
     */
    private $trackerFactory;

    public function __construct(ScopeConfigInterface $configInterface, TrackerFactory $trackerFactory)
    {
        $this->configInterface = $configInterface;
        $this->trackerFactory = $trackerFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $siteId = $this->configInterface->getValue('mootracker_site_id_section/mootracker_group_site_id/mootracker_site_id');

        if(empty($siteId)){
            return;
        }

        $eventData = $observer->getEvent()->getData();
        $customer = $eventData['customer']->getDataModel();

        $name = $customer->getFirstname() . ' ' . $customer->getLastname();
        $email = $customer->getEmail();

        $tracker = $this->trackerFactory->create($siteId);

        if (!$tracker->isIdentified($email)) {
            try {
                $tracker->identify($email, $name);
            } catch (Exception $err) {
                trigger_error('Could not track events for MooTracker', E_USER_WARNING);
            }
        }
    }
}