<?php namespace Moosend\MagentoMooTracker\Model;

use \Magento\Framework\Event\Observer;
use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use \Moosend\TrackerFactory;

class FrontSendResponseObserver implements ObserverInterface
{

    /**
     * @var ScopeConfigInterface
     */
    private $configInterface;

    /**
     * @var TrackerFactory
     */
    private $trackerFactory;

    public function __construct(ScopeConfigInterface $configInterface, TrackerFactory $trackerFactory)
    {
        $this->configInterface = $configInterface;
        $this->trackerFactory = $trackerFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(Observer $observer)
    {
        $siteId = $this->configInterface->getValue('mootracker_site_id_section/mootracker_group_site_id/mootracker_site_id');

        if(empty($siteId)){
            return;
        }

        $eventData = $observer->getEvent()->getData();
        $request = $eventData['request'];

        $tracker = $this->trackerFactory->create($siteId);

        $tracker->init($siteId);

        //Track page view
        if (! $request->isAjax()) {
            try {
                $actual_link = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
                $tracker->pageView($actual_link);
            } catch (Exception $err) {
                trigger_error('Could not track events for MooTracker', E_USER_WARNING);
            }
        }
    }
}