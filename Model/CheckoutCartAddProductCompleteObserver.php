<?php namespace Moosend\MagentoMooTracker\Model;

use \Magento\Framework\Event\ObserverInterface;
use \Magento\Framework\App\Config\ScopeConfigInterface;
use Moosend\TrackerFactory;

class CheckoutCartAddProductCompleteObserver implements ObserverInterface
{

    /**
     * @var ScopeConfigInterface
     */
    protected $configInterface;
    /**
     * @var TrackerFactory
     */
    private $trackerFactory;

    public function __construct(ScopeConfigInterface $configInterface, TrackerFactory $trackerFactory)
    {
        $this->configInterface = $configInterface;
        $this->trackerFactory = $trackerFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $siteId = $this->configInterface->getValue('mootracker_site_id_section/mootracker_group_site_id/mootracker_site_id');

        if(empty($siteId)){
            return;
        }

        $evetnData = $observer->getEvent()->getData();
        $product = $evetnData['product'];
        $tracker = $this->trackerFactory->create($siteId);

        $price = $product->getPrice();
        /*$sku = $product->getSku();*/
        $id = $product->getId();
        $name = $product->getName();

        try {
            $tracker->addToOrder($id, $price, ['name' => $name]);
        } catch (Exception $err) {
            trigger_error('Could not track events for MooTracker', E_USER_WARNING);
        }
    }
} 