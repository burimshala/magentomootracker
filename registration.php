<?php namespace Moosend\MagentoMooTracker;

\Magento\Framework\Component\ComponentRegistrar::register(
    \Magento\Framework\Component\ComponentRegistrar::MODULE,
    'MooTracker',
    __DIR__
);