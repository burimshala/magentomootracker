<?php namespace Moosend\MagentoMooTracker\Model;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Sales\Model\OrderFactory;
use Moosend\TrackerFactory;

class CheckoutOnepageControllerSuccessObserver implements ObserverInterface
{

    /**
     * @var ScopeConfigInterface
     */
    protected $configInterface;
    /**
     * @var TrackerFactory
     */
    private $trackerFactory;
    /**
     * @var OrderFactory
     */
    private $orderFactory;

    public function __construct(OrderFactory $orderFactory, ScopeConfigInterface $configInterface, TrackerFactory $trackerFactory)
    {
        $this->orderFactory = $orderFactory;
        $this->configInterface = $configInterface;
        $this->trackerFactory = $trackerFactory;
    }

    /**
     * @param \Magento\Framework\Event\Observer $observer
     * @return void
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {

        $siteId = $this->configInterface->getValue('mootracker_site_id_section/mootracker_group_site_id/mootracker_site_id');

        if(empty($siteId)){
            return;
        }

        $eventData = $observer->getEvent()->getData();
        $orderIds = $eventData['order_ids'];

        $tracker = $this->trackerFactory->create($siteId);

        if($orderIds){
            foreach ($orderIds as $orderId) {

                $order = $this->orderFactory->create()->load($orderId);
                $items = $order->getItems();

                $products = array_map(function($item){
                    $product = $item->getProduct();

                    return [
                        'itemCode' => $product->getId(),
                        'itemPrice' => $product->getPrice(),
                        'name' => $product->getName()
                    ];
                }, $items);

                try {
                    $tracker->orderCompleted(['products' => $products]);
                } catch (Exception $err) {
                    trigger_error('Could not track events for MooTracker', E_USER_WARNING);
                }
            }
        }
    }
} 