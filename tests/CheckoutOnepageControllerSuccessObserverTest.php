<?php

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event;
use Moosend\MagentoMooTracker\Model\CheckoutOnepageControllerSuccessObserver;
use Magento\Framework\Event\Observer;
use Magento\Sales\Model\OrderFactory;
use Magento\Sales\Model\AbstractModel;
use Magento\Catalog\Model\Product\Interceptor;
use Moosend\Tracker;
use Moosend\TrackerFactory;

class CheckoutOnepageControllerSuccessObserverTest extends PHPUnit_Framework_TestCase
{
    protected $checkoutOnepageControllerSuccessObserver;
    protected $tracker;

    protected function setUp()
    {
        //Mocks
        $scopeConfiguration = $this->getMockBuilder(ScopeConfigInterface::class)->getMock();
        $scopeConfiguration->method('getValue')->willReturn('some-site-id');

        $product = $this->getMockBuilder(Interceptor::class)->setMethods(['getId', 'getPrice', 'getName'])->getMock();
        $product->method('getId')->willReturn('0000-product-id');
        $product->method('getPrice')->willReturn('12');
        $product->method('getName')->willReturn('Random Product');

        $orderItem = $this->getMockBuilder(AbstractModel::class)->disableOriginalConstructor()->setMethods(['getProduct'])->getMock();
        $orderItem->method('getProduct')->willReturn($product);

        $loadedOrder = $this->getMockBuilder(AbstractModel::class)->disableOriginalConstructor()->setMethods(['getItems'])->getMock();
        $loadedOrder->method('getItems')->willReturn([$orderItem]);

        $order = $this->getMockBuilder(AbstractModel::class)->disableOriginalConstructor()->getMock();
        $order->method('load')->willReturn($loadedOrder);

        $orderFactory = $this->getMockBuilder(OrderFactory::class)->setMethods(['create'])->getMock();
        $orderFactory->method('create')->willReturn($order);

        $this->tracker = $this->getMockBuilder(Tracker::class)
            ->disableOriginalConstructor()
            ->getMock();

        $trackerFactory = $this->getMockBuilder(TrackerFactory::class)->getMock();
        $trackerFactory->method('create')->willReturn($this->tracker);

        $this->checkoutOnepageControllerSuccessObserver = new CheckoutOnepageControllerSuccessObserver($orderFactory, $scopeConfiguration, $trackerFactory);
    }

    function test_it_tracks_order_complete()
    {
        //Mocks
        $observer = $this->getMockBuilder(Observer::class)->getMock();
        $event = $this->getMockBuilder(Event::class)->getMock();

        $event->method('getData')->willReturn(['order_ids' => ['1']]);
        $observer->method('getEvent')->willReturn($event);

        $this->tracker->expects($this->once())->method('orderCompleted');

        $this->checkoutOnepageControllerSuccessObserver->execute($observer);
    }
} 