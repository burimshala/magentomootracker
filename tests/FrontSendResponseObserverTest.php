<?php

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event;
use Moosend\MagentoMooTracker\Model\FrontSendResponseObserver;
use Magento\Framework\App\Request\Http;
use Magento\Framework\Event\Observer;
use Moosend\Tracker;
use Moosend\TrackerFactory;

class FrontSendResponseObserverTest extends PHPUnit_Framework_TestCase
{

    protected $frontSendResponseObserver;
    protected $tracker;

    protected function setUp()
    {
        //Mocks
        $scopeConfiguration = $this->getMockBuilder(ScopeConfigInterface::class)->getMock();
        $scopeConfiguration->method('getValue')->willReturn('some-site-id');

        $trackerFactory = $this->getMockBuilder(TrackerFactory::class)->getMock();

        $this->tracker = $this->getMockBuilder(Tracker::class)
            ->disableOriginalConstructor()
            ->getMock();

        $trackerFactory->method('create')->willReturn($this->tracker);

        $this->frontSendResponseObserver = new FrontSendResponseObserver($scopeConfiguration, $trackerFactory);
    }

    public function test_it_tracks_page_views()
    {
        //Mocks
        $observer = $this->getMockBuilder(Observer::class)->getMock();
        $event = $this->getMockBuilder(Event::class)->getMock();
        $request = $this->getMockBuilder(Http::class)->disableOriginalConstructor()->getMock();

        //Stubs
        $request->method('isAjax')->willReturn(false);
        $observer->method('getEvent')->willReturn($event);
        $event->method('getData')->willReturn(['request' => $request]);

        $_SERVER['HTTP_HOST'] = '';
        $_SERVER['REQUEST_URI'] = '';

        //Expects
        $this->tracker->expects($this->once())->method('init');
        $this->tracker->expects($this->once())->method('pageView');

        $this->frontSendResponseObserver->execute($observer);
    }
} 