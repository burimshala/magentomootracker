<?php

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event;
use Moosend\MagentoMooTracker\Model\CustomerLoginObserver;
use Magento\Customer\Model\Customer;
use Magento\Framework\Event\Observer;
use Moosend\Tracker;
use Moosend\TrackerFactory;

class CustomerLoginObserverTest extends PHPUnit_Framework_TestCase{

    protected $customerLoginObserver;
    protected $tracker;

    protected function setUp()
    {
        //Mocks
        $scopeConfiguration = $this->getMockBuilder(ScopeConfigInterface::class)->getMock();
        $scopeConfiguration->method('getValue')->willReturn('some-site-id');

        $trackerFactory = $this->getMockBuilder(TrackerFactory::class)->getMock();

        $this->tracker = $this->getMockBuilder(Tracker::class)
            ->disableOriginalConstructor()
            ->getMock();

        $trackerFactory->method('create')->willReturn($this->tracker);

        $this->customerLoginObserver = new CustomerLoginObserver($scopeConfiguration, $trackerFactory);
    }

    public function test_it_tracks_identify()
    {
        //Mocks
        $observer = $this->getMockBuilder(Observer::class)->getMock();
        $event = $this->getMockBuilder(Event::class)->getMock();
        $customer = $this->getMockBuilder(Customer::class)->disableOriginalConstructor()->getMock();
        $customerDataModel = $this->getMockBuilder(Customer::class)->disableOriginalConstructor()->getMock();

        //Stubs
        $observer->method('getEvent')->willReturn($event);

        $customerDataModel->method('getFirstname')->willReturn('John');
        $customerDataModel->method('getLastname')->willReturn('Doe');
        $customerDataModel->method('getEmail')->willReturn('some@mail.com');

        $customer->method('getDataModel')->willReturn($customerDataModel);

        $event->method('getData')->willReturn(['customer' => $customer]);

        $this->tracker->method('isIdentified')->willReturn(false);

        //Expects
        $this->tracker->expects($this->once())->method('identify');

        $this->customerLoginObserver->execute($observer);
    }
} 