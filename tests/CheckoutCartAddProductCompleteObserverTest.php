<?php

use Magento\Framework\App\Config\ScopeConfigInterface;
use Magento\Framework\Event;
use Moosend\MagentoMooTracker\Model\CheckoutCartAddProductCompleteObserver;
use Magento\Framework\Event\Observer;
use Magento\Catalog\Model\Product\Interceptor;
use Moosend\Tracker;
use Moosend\TrackerFactory;

class CheckoutCartAddProductCompleteObserverTest extends PHPUnit_Framework_TestCase{

    protected $checkoutCartAddProductCompleteObserver;
    protected $tracker;

    protected function setUp()
    {
        //Mocks
        $scopeConfiguration = $this->getMockBuilder(ScopeConfigInterface::class)->getMock();
        $scopeConfiguration->method('getValue')->willReturn('some-site-id');

        $trackerFactory = $this->getMockBuilder(TrackerFactory::class)->getMock();

        $this->tracker = $this->getMockBuilder(Tracker::class)
            ->disableOriginalConstructor()
            ->getMock();

        $trackerFactory->method('create')->willReturn($this->tracker);

        $this->checkoutCartAddProductCompleteObserver = new CheckoutCartAddProductCompleteObserver($scopeConfiguration, $trackerFactory);
    }

    public function test_it_tracks_add_to_order_events()
    {
        //Mocks
        $observer = $this->getMockBuilder(Observer::class)->getMock();
        $event = $this->getMockBuilder(Event::class)->getMock();
        $product = $this->getMockBuilder(Interceptor::class)->setMethods(['getId', 'getPrice', 'getName'])->getMock();

        //Stubs
        $product->method('getId')->willReturn('0000-product-id');
        $product->method('getPrice')->willReturn('12');
        $product->method('getName')->willReturn('Random Product');

        $observer->method('getEvent')->willReturn($event);
        $event->method('getData')->willReturn(['product'=>$product]);

        $this->tracker->expects($this->once())->method('addToOrder');

        $this->checkoutCartAddProductCompleteObserver->execute($observer);
    }
} 